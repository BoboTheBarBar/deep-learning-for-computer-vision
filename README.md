# Readme
Quelle: https://open.hpi.de/courses/neuralnets2020
Dieses Projekt sind meine Lösungen zum „Deep Learning for Computer Vision” Kurs von 2020. Hier wurde Tensorflow und Keras lokal ein wenig gequält um Convolutional NN zur Bilderkennung zu verwenden und sukzessive zu verbessern. Bis hin zur Einbindung von transfer learning aus bestehenden guten Netzten.
