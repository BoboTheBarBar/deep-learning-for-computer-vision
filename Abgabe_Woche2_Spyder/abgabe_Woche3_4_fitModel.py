# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 21:36:31 2020

@author: Boris
"""


import abgabe_Woche3_4


train_data, test_data, classes = ImageWoof.load_data()

# Durchmischen der Trainingsdaten, dass nicht mit sortierten Bildern trainiert wird 
train_data = train_data.shuffle(1000) 

print('shape des Trainigsdatensatzes vor dem preprocessing: ', train_data)

train_data = train_data.map(preprocess).batch(batch_size).prefetch(1)          
test_data = test_data.map(preprocess).batch(batch_size).prefetch(1)

print('shape des Traingingsdatensatzes nach dem preprocessing: ', train_data)


# Training des Modells
history = CNN_model.fit(train_data, epochs=12, validation_data=test_data)
helpers.plot_history('First net performance', history, 0)