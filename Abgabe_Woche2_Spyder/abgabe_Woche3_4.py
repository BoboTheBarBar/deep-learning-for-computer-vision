# -*- coding: utf-8 -*-
"""
Created on Thu Apr  2 21:21:30 2020

@author: Boris
"""

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Dense, Activation, Input, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.models import Model
from deeplearning2020 import helpers
from deeplearning2020.datasets import ImageWoof

assert tf.__version__ >= "2.0"

if not tf.test.is_gpu_available():
    print("No GPU was detected. CNNs can be very slow without a GPU.")


def preprocess(image, label):
    resized_image = tf.image.resize(image, [300, 300])
    return resized_image, label

# Layers: Convolutional Neural Network (CNN) mit MaxPooling und DenseLayer
def addConvAndMaxPooling(oldModel, kernel_size, filterNumber, customPadding='same', activationFunction = 'relu', maxPoolingSize = 2):
    oldModel = Conv2D(
        filters=filterNumber,
        kernel_size=(kernel_size, kernel_size),
        activation=activationFunction,
        padding=customPadding
    )(oldModel)
    oldModel = MaxPooling2D((maxPoolingSize, maxPoolingSize))(oldModel)
    return oldModel

def addDenseLayer(oldModel, neurons, activationFunction):
    return Dense(
        neurons,
        activation=activationFunction
    )(oldModel)

# Festlegung der Batch Größe für die Datenvorbereitung
batch_size = 32
n_classes = 10

# model
learning_rate=0.01
momentum=0.9
dense_neurons=500
n_filters=512
first_kernel_size=(7,7)


train_data, test_data, classes = ImageWoof.load_data()

# Durchmischen der Trainingsdaten, dass nicht mit sortierten Bildern trainiert wird 
train_data = train_data.shuffle(1000) 

print('shape des Trainigsdatensatzes vor dem preprocessing: ', train_data)

train_data = train_data.map(preprocess).batch(batch_size).prefetch(1)          
test_data = test_data.map(preprocess).batch(batch_size).prefetch(1)

print('shape des Traingingsdatensatzes nach dem preprocessing: ', train_data)



# Inputgröße muss 300x300 Pixel mit 3 RGB Farben betragen
input_layer = Input(shape=(300, 300, 3))

# 6 Convolutional Layers mit jeweils einer Max Pooling Layer
model = addConvAndMaxPooling(input_layer, 7, 64)
model = addConvAndMaxPooling(model, 3, 256)
model = addConvAndMaxPooling(model, 3, n_filters)
model = addConvAndMaxPooling(model, 3, n_filters)
model = addConvAndMaxPooling(model, 3, n_filters)
model = addConvAndMaxPooling(model, 3, n_filters)
#model = addConvAndMaxPooling(model, 3, n_filters)

# Fully-Connected-Classifier
model = Flatten()(model)
model = addDenseLayer(oldModel = model, neurons = dense_neurons, activationFunction = 'relu')
model = addDenseLayer(oldModel = model, neurons = dense_neurons / 2, activationFunction = 'tanh')

# Output Layer
output = addDenseLayer(oldModel = model, neurons = n_classes, activationFunction = "softmax")

CNN_model = Model(input_layer, output)

# Kompilieren des Modells
optimizer = keras.optimizers.SGD(lr=learning_rate, momentum=momentum)
CNN_model.compile(loss ="sparse_categorical_crossentropy", optimizer=optimizer, metrics=["accuracy"])
CNN_model.summary()

# Training des Modells
history = CNN_model.fit(train_data, epochs=12, validation_data=test_data)
helpers.plot_history('First net performance', history, 0)