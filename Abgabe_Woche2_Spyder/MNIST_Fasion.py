# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 15:33:32 2020

@author: Boris
"""


#!pip install deeplearning2020
import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
from deeplearning2020 import helpers

# data = keras.datasets.mnist
# (train_images, train_labels), (test_images, test_labels) = data.load_data()

data = keras.datasets.fashion_mnist

(train_images, train_labels), (test_images, test_labels) = data.load_data()

train_images = train_images / 255.0
test_images = test_images / 255.0

total_classes = 10
train_vec_labels = keras.utils.to_categorical(train_labels, total_classes)
test_vec_labels = keras.utils.to_categorical(test_labels, total_classes)

model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28, 28)),
    keras.layers.Dense(256, activation='relu'), 
    keras.layers.Dense(128, activation='relu'), 
    keras.layers.Dense(10, activation='tanh')
])

# sgd = keras.optimizers.SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(
    optimizer='sgd',
    loss='mean_squared_error',
    metrics=['accuracy'])

model.fit(train_images, train_vec_labels, epochs=10, verbose=True)

eval_loss, eval_accuracy = model.evaluate(test_images, test_vec_labels, verbose=False)
print("Model accuracy: %.2f" % eval_accuracy)

helpers.plot_predictions(model, test_images[:20], labels=test_vec_labels[:20])